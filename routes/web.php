<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Test command bus implementation route
//
// Route::get('command-bus-test', [
//   'as' => 'commandBusTest',
//   'uses' => 'Controller@commandBusTest'
// ]);

Route::get('/', [
  'as' => 'home.index',
  function () {
      return view('home.index');
  },
]);
