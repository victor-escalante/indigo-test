/** Variables and Constants **/

const elixir = require('laravel-elixir');

require('laravel-elixir-bless');

/* User less or sass */
const compiler = 'less';

const assets_large_dir = 'public/assets/'
const assets_short_dir = 'public/'

// ---

/** Scripts **/

var baseJs = [

  /** Bootstrap **/

  /* Uncomment those you need */
  // '../../../node_modules/bootstrap/js/transition.js',
  // '../../../node_modules/bootstrap/js/alert.js',
  // '../../../node_modules/bootstrap/js/button.js',
  // '../../../node_modules/bootstrap/js/carousel.js',
  // '../../../node_modules/bootstrap/js/collapse.js',
  // '../../../node_modules/bootstrap/js/dropdown.js',
  // '../../../node_modules/bootstrap/js/modal.js',
  // '../../../node_modules/bootstrap/js/tooltip.js',
  // '../../../node_modules/bootstrap/js/popover.js',
  // '../../../node_modules/bootstrap/js/scrollspy.js',
  // '../../../node_modules/bootstrap/js/tab.js',
  // '../../../node_modules/bootstrap/js/affix.js',

  /** Plugins **/

  /* Uncomment if you are using Owl Carousel */
  //'../bower_components/owl.carousel/dist/owl.carousel.min.js',


  /** Base **/

  'base.js'
];

var appJs = [
  'app.js',
];

// ---

/** Copy **/

var copyJs = [
  'node_modules/jquery/dist/jquery.min.js',
  'resources/assets/bower_components/modernizr/modernizr.js'
];


var copyFonts = [
  'resources/assets/bower_components/font-awesome/fonts',
];


/** Version **/

var versionFile = [
  assets_large_dir + 'css/app.css',
  assets_large_dir + 'js/base.js',
  assets_large_dir + 'js/app.js'
];

// ---

/** Compile and mixing **/

elixir(function(mix){

  /** Styles **/

  if (compiler == 'sass'){
    mix.sass([
      'app.scss'
    ], assets_large_dir + 'css')
  }

  if (compiler == 'less'){
    mix.less([
      'app.less'
    ], assets_large_dir + 'css')
  }

  mix.bless(
    assets_large_dir + 'css/app.css',
    assets_large_dir + 'css',
    {
      imports: true,
    }
  );

  // ---

  mix

  /** Scripts **/

  .scripts(
    baseJs,
    assets_large_dir + 'js/base.js'
  )

  .scripts(
    appJs,
    assets_large_dir + 'js/app.js'
  )

  // ---

  /** Copy **/

  .copy(
    copyJs,
    assets_large_dir + 'js/vendor'
  )

  .copy(
    copyFonts,
    assets_large_dir + 'fonts/vendor'
  )

  /* Build */

  .copy(
    copyFonts,
    assets_short_dir + 'build/assets/fonts/vendor'
  )

  .copy(
    assets_large_dir + 'css',
    assets_short_dir + 'build/assets/css'
  )

  /** Version **/

  .version(versionFile);

  // ---

});
