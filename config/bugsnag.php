<?php

/*
 * This file is part of Alt Three Bugsnag.
 *
 * (c) Alt Three Services Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | API Key
    |--------------------------------------------------------------------------
    |
    | This api key points the bugsnag notifier to the project in your account.
    |
    */

    'key' => env('BUGSNAG_API_KEY', ''),

    'notify_release_stages' => ['development', 'production'],

];
