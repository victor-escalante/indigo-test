**Test Indigo**

Autor - Víctor Escalante Núñez
2017-11-08 00:45:58 Wednesday

Note:
This proyect is created in Laravel

# Install

1) Clone repository

2) Install dependences (npm and composer ).
**$** ` npm install`
**$** `composer install`
**$** `bower install`

3) Create a copy of the file **.env.example** to **.env**
**$** ` cp  .env.example .env`

4) Generate a application key
**$** `php artisan key:generate`

5) Compile Assets
**$** `gulp`

## Visit examples

1)  Example 1 - Largest product in a series
`http://localhost/largest-product`

2) Example 2 - Largest palindrome product
`http://localhost/largest-palindrome`
