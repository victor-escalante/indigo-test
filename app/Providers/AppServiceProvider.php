<?php

namespace App\Providers;

use AltThree\Bus\Dispatcher;
use App\Http\Middleware\DatabaseTransactionsMiddleware;
use App\Http\Middleware\ValidationMiddleware;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $dispatcher)
    {
        $dispatcher->mapUsing(function ($command) {
            return Dispatcher::simpleMapping($command, 'App\Bus', 'App\Bus\Handlers');
        });

        $dispatcher->pipeThrough([ValidationMiddleware::class, DatabaseTransactionsMiddleware::class]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('bugsnag.multi', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.multi', \Psr\Log\LoggerInterface::class);
    }
}
