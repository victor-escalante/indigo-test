<?php

namespace App\Exceptions;

use Illuminate\Contracts\Support\MessageProvider;
use Illuminate\Support\MessageBag;
use RuntimeException;

/**
 * This is the validating exception class.
 *
 */
class ValidationException extends RuntimeException implements MessageProvider
{
    /**
     * The validation errors.
     *
     * @var \Illuminate\Contracts\Support\MessageBag
     */
    protected $errors;

    /**
     * Makes a new validation exception instance.
     *
     * @param \Illuminate\Contracts\Support\MessageBag $errors
     * @param \App\Models\null                         $model
     *
     * @return void
     */
    public function __construct(MessageBag $errors, $model = null)
    {
        $this->errors = $errors;

        parent::__construct(sprintf('Validation%s has failed.', $model ? " for '{$model->object}'" : ''));
    }

    /**
     * Get the messages for the instance.
     *
     * @return \Illuminate\Contracts\Support\MessageBag
     */
    public function getMessageBag()
    {
        return $this->errors;
    }
}
