<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /*
     * Test command bus implementation
     *
     * @return boolean
     */
    // public function commandBusTest()
    // {
    //     try {
    //         $test = $this->dispatch(new \App\Bus\Commands\Test\TestCommand('Test Command dispatched and handled'));
    //     } catch (ValidationException $e) {
    //         return $e->getMessage();
    //     }

    //     dd($test);
    // }
}
