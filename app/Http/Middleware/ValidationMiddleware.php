<?php

namespace App\Http\Middleware;

use App\Exceptions\ValidationException;
use Closure;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\MessageBag;
use Illuminate\Translation\Translator;
use ReflectionClass;

/**
 * The validation middleware class.
 *
 */
class ValidationMiddleware
{
    /**
     * The validation factory instance.
     *
     * @var \Illuminate\Contracts\Validation\Factory
     */
    protected $factory;

    /**
     * @var \Illuminate\Translation\Translator
     */
    protected $translator;

    /**
     * Create a new validating middleware instance.
     *
     * @param \Illuminate\Contracts\Validation\Factory $factory
     * @param \Illuminate\Translation\Translator       $translator
     */
    public function __construct(Factory $factory, Translator $translator)
    {
        $this->factory = $factory;
        $this->translator = $translator;
    }

    /**
     * Validate the command before execution.
     *
     * @param object   $command
     * @param \Closure $next
     *
     * @throws \App\Exceptions\ValidationException
     *
     * @return void
     */
    public function handle($command, Closure $next)
    {
        if (property_exists($command, 'rules') && is_array($command->rules)) {
            $this->validate($command);
        }

        return $next($command);
    }

    /**
     * Validate the command.
     *
     * @param object $command
     *
     * @throws \App\Exceptions\ValidationException
     *
     * @return void
     */
    protected function validate($command)
    {
        $messages = property_exists($command, 'validationMessages') ? $command->validationMessages : [];

        $validator = $this->factory->make($this->getData($command), $command->rules, $messages);

        if ($validator->fails()) {
            $messageBag = $validator->getMessageBag();
            $newMessages = [];

            // each message returns the field as key and an array as error messages
            foreach ($messageBag->getMessages() as $field => $errorMessages) {

                // we will iterate through the error messages array, translate the key
                // and replace it in the messages
                foreach ($errorMessages as $errorMessage) {
                    if ($this->translator->has("validationfields.{$field}")) {
                        $translatedField = $this->translator->trans("validationfields.{$field}");
                    }
                    // if not found try again but transforming first to snake_case
                    // this b/c the field may come from a camelCase command
                    else {
                        $snakeField = snake_case($field);
                        if ($this->translator->has("validationfields.{$snakeField}")) {
                            $translatedField = $this->translator->trans("validationfields.{$snakeField}");
                        }
                        // if key not found assign the name of the field
                        else {
                            // TODO log if key not found
                            $translatedField = $field;
                        }
                    }

                    $newMessage = str_replace($field, $translatedField, $errorMessage);

                    // Patch: if the new message is the same that the previous one
                    // is mostly because laravel humanized a camel case string, so try
                    // again snake-casing the key and then humanizing it replacing '_' by ' '
                    if ($newMessage == $errorMessage) {
                        $humanizedField = str_replace('_', ' ', snake_case($field));
                        $newMessage = str_replace($humanizedField, $translatedField, $errorMessage);
                    }

                    $newMessages[$field][] = $newMessage;
                }
            }

            throw new ValidationException(new MessageBag($newMessages));
        }

        if (method_exists($command, 'validate')) {
            $command->validate();
        }
    }

    /**
     * Get the data to be validated.
     *
     * @param object $command
     *
     * @return array
     */
    protected function getData($command)
    {
        $data = [];

        foreach ((new ReflectionClass($command))->getProperties() as $property) {
            $property->setAccessible(true);

            $name = $property->getName();
            $value = $property->getValue($command);

            if (in_array($name, ['rules', 'validationMessages'], true) || is_object($value)) {
                continue;
            }

            $data[$name] = $value;
        }

        return $data;
    }
}
