<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\DatabaseManager;

/**
 * This is the database transactions middleware class.
 *
 */
class DatabaseTransactionsMiddleware
{
    /**
     * The database manager instance.
     *
     * @var \Illuminate\Database\DatabaseManager
     */
    protected $db;

    /**
     * Create a new validating middleware instance.
     *
     * @param \Illuminate\Database\DatabaseManager $db
     *
     * @return void
     */
    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * Validate the command before execution.
     *
     * @param object   $command
     * @param \Closure $next
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function handle($command, Closure $next)
    {
        if (property_exists($command, 'databaseTransactions') && $command->databaseTransactions === true) {
            return $this->db->connection()->transaction(function () use ($command, $next) {
                return $next($command);
            });
        }

        return $next($command);
    }
}
