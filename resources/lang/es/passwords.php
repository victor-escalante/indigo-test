<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe ser al menos de 6 caracteres y ser igual a la confirmación.',
    'reset'    => 'Su contraseña ha sido reestablecida',
    'sent'     => 'Le hemos enviado un correo con el link para reestablecer su contraseña',
    'token'    => 'El token para reestablecer su contraseña es inválido',
    'user'     => 'No se encontró el usuario correspondiente al Email proporcionado',

];
