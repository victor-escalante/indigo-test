<?php

  $container_class = (empty($container_class)) ? '' : $container_class;
  $value = (empty($value)) ? old($id_name, '') : $value;
  $options = (empty($options)) ? [] : $options;
  $attributes = (empty($attributes)) ? [] : $attributes;
  $label = (empty($label)) ? '' : $label;
  $item_selected = (!isset($item_selected)) ? old($id_name, '') : $item_selected;
  $select_class = (empty($select_class)) ? '' : $select_class;
  $n_a = (empty($n_a)) ? '' : $n_a;
  $ico = (empty($ico)) ? '' : $ico;
  $with_ico = '';

  //--

  if ($ico) {
      $with_ico = 'with-ico';
  }

?>

<div class="input-field select {{ $container_class }} {{ $with_ico }}">

  <select
  id="{{ $id_name }}"
  name="{{ $id_name }}"
  class="{{ $select_class }} {{ ($errors->has($id_name)) ? 'error' : '' }}"
  @foreach($attributes as $attr => $attribute)
    {{ $attr }}='{{ $attribute }}'
  @endforeach
  >

    <option
      value=""
      disabled=""
      selected style="color:#f00;
      display:none;">
    </option>

    @if(empty($n_a))

      {{-- <option value="n/a">N/A</option> --}}

    @endif

    @foreach($options as $key => $value)

      <option
        {{ ($key != 'null') ? '' : 'disabled ' }}
        value="{{ $key }}"
        {{ ($item_selected == $key) ? 'selected' : ''}}>
        {{ $value }}
      </option>

    @endforeach

  </select>

  <label>
    {!! ($ico) ? "<i class='fa fa-".$ico."'></i>" : '' !!}
    {!! $label !!}
  </label>

</div>


