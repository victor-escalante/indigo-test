<?php

  $type = (empty($type)) ? 'text' : $type;
  $autocomplete = (empty($autocomplete)) ? 'on' : $autocomplete;
  $disabled = (empty($disabled)) ? '' : 'disabled';
  $checked = (empty($checked)) ? '' : 'checked';
  $container_class = (empty($container_class)) ? '' : $container_class;
  $value = (empty($value)) ? old($id_name, '') : $value;
  $ico = (empty($ico)) ? '' : $ico;
  $label = (empty($label)) ? '' : $label;
  $with_ico = '';
  $index = (empty($index)) ? '' : '-'.$index;

  //--

  if ($ico) {
      $with_ico = 'with-ico';
  }

?>

@if($type == 'hidden')

  <input
  id="{{ $id_name }}"
  type="{{ $type }}"
  name="{{ $id_name }}"
  value="{{ $value }}"
  {{ $disabled }}
  class="{{ $input_class }}">

@elseif($type == 'file')

  <div class="input-field {{ $type }} {{ $container_class }} {{ $with_ico }}">
    <input
    id="{{ $id_name }}"
    type="{{ $type }}"
    name="{{ $id_name }}"
    class="{{ ($errors->has($id_name)) ? 'error' : '' }} {{ ($value) ? 'active' : '' }}"
    {{ $disabled }}>
    <label for="{{ $id_name }}">{{ $label }}</label>
    {!! ($ico) ? "<i class='fa fa-".$ico."'></i>" : '' !!}
  </div>

@elseif($type == 'radio' || $type == 'checkbox')

  <div class="input-field {{ $type }} {{ $container_class }} {{ $with_ico }}">
    <input
    type="{{ $type }}"

    id="{{ $id_name }}{{ $index }}"
    name="{{ $id_name }}"
    class="{{ ($errors->has($id_name)) ? 'error' : '' }} {{ ($value) ? 'active' : '' }}"
    value="{{ $value }}"
    {{ $disabled }}
    {{ $checked }}>
    <label for="{{ $id_name }}{{ $index }}">{!! $label !!}</label>
  </div>

@else

  <div class="input-field {{ $container_class }} {{ $with_ico }}">
    <input
    type="{{ $type }}"
    id="{{ $id_name }}"
    name="{{ $id_name }}"
    autocomplete="{{ $autocomplete }}"
    {{ $disabled }}
    class="{{ ($errors->has($id_name)) ? 'error' : '' }} {{ ($value) ? 'active' : '' }}"
    value="{{ $value }}">
    <label>{!! $label !!}</label>

    {!! ($ico) ? "<i class='fa fa-".$ico."'></i>" : '' !!}
  </div>

@endif
