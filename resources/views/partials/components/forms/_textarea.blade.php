<?php

  $disabled = (empty($disabled)) ? '' : 'disabled';
  $container_class = (empty($container_class)) ? '' : $container_class;
  $value = (empty($value)) ? old($id_name, '') : $value;
  $label = (empty($label)) ? '' : $label;

?>

<div
	class="input-field textarea {{ $container_class }}">

  <textarea
	  rows="1"
	  id="{{ $id_name }}"
	  name="{{ $id_name }}"
	  class="{{ ($errors->has($id_name)) ? 'error' : '' }}"
	  {{ $disabled }}>{{ $value }}</textarea>

  <label>{!! $label !!}</label>

</div>


