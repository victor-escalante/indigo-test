@if (!empty($id) && env('APP_ENV', 'local') == 'production')

  <?php

    $id = (empty($id) ? '' : $id);

  ?>

  <!-- Google Tag Manager -->
    <noscript>
      <iframe
        src="https://www.googletagmanager.com/ns.html?id=GTM-{{ $id }}"
        height="0"
        width="0"
        style="display:none;visibility:hidden">
      </iframe>
    </noscript>
  <!-- End Google Tag Manager -->

@endif
