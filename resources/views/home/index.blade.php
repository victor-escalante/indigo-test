@extends('layouts.master')
@section('title', 'Indigo')
@section('body-id', 'home')
@section('main-class', 'home')

@section('content')

  @include('home.partials._default')

@stop

@section('js')

  <script>

    // --

  </script>

@stop
