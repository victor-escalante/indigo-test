<!DOCTYPE html>

<!--
  __              __      __          __
 /\ \  __        /\ \    /\ \      __/\ \__
 \_\ \/\_\    ___\ \ \/'\\ \ \____/\_\ \ ,_\
 /'_` \/\ \ /' _ `\ \ , < \ \ '__`\/\ \ \ \/
/\ \L\ \ \ \/\ \/\ \ \ \\`\\ \ \L\ \ \ \ \ \_
\ \___,_\ \_\ \_\ \_\ \_\ \_\ \_,__/\ \_\ \__\
 \/__,_ /\/_/\/_/\/_/\/_/\/_/\/___/  \/_/\/__/
 ::{{ env('APP_AUTHOR') }}

-->

<!--[if lte IE 7 ]> <html class="ie6 ie7 ielt9"> <![endif]-->
<!--[if IE 8 ]>     <html class="ie8 ielt9"> <![endif]-->
<!--[if IE 9 ]>     <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="es"> <!--<![endif]-->

@include('layouts.partials._head')

  <body id="@yield('body-id', '')">

    <!--[if lte IE 8]>
      <p class="chromeframe">Estás usando un navegador <strong>antiguo</strong>. Por favor <a href="http://browsehappy.com/">actualiza tu navegador</a> para visualizar el sitio correctamente.</p>
    <![endif]-->

    @include('partials.components.google._tag-manager-body', ['id' => ''])

    <div class="smain-container container-fluid @yield('main-class', '')">

      @include('layouts.partials._nav')
      @include('layouts.partials._messages')
      @yield('content')

    </div>

    <footer>
      @include('layouts.partials._footer')
    </footer>

    <script src="{{ elixir('/assets/js/app.js') }}"></script>

    @yield('js')

    <div class="alert alert-danger noJS" style="display:none;">
      Le recomendamos habilitar el uso de javascript en su navegador para mejorar su experiencia en nuestra plataforma
    </div>

  </body>
</html>
