<?php

  if (Request::get('success') || Session::has('success')) {
      if (Request::get('success')) {
          $success = Request::get('success');
      } elseif (Session::has('success')) {
          $success = Session::get('success');
      }

      $id = 'success';
      $type = 'success';
      $title = '¡Exito!';
  }

  if ($errors->any()) {
      $id = 'alertas';
      $type = 'danger';
      $title = 'Algo ocurrió';
  }

?>

@if($errors->any() || isset($success))

  <div class="messages">

    <div id="{{ $id }}" class="alert alert-{{ $type }}">

      <div class="state {{ $type }}">

        <div class="line">

          <div class="scol-x-9">
            <p class="txt-bold fs-20">{{ $title }}</p>
          </div>

          <div class="scol-x-3 right-x">
            <a class="close"><i class="fa fa-times-circle-o"></i></a>
          </div>

        </div>

      </div>

      @if($errors->any())

        <ul>

          @foreach ($errors->all() as $error)

            <li>{!! $error !!}</li>

          @endforeach

        </ul>

      @else

        @if($success === true || $success == 1)

          <p>La operación ha sido realizada exitosamente</p>

        @else

          <p>{!! $success !!}</p>

        @endif

      @endif

    </div>

  </div>

@endif
