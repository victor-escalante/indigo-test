/* * * * * * * * * * * * */
/* Initialize functions  */
/* * * * * * * * * * * * */

  initialize = {

    docReady: function() {

      /** Custom **/

        // ...

      /* * * * * * */

      /** Default **/
        fx_messages.init(); // base.js
      /* * * * * * */
    },

    winLoad: function() {

      /** Custom **/

        // ...

      /* * * * * * */

      /** Default **/

         // ...

      /* * * * * * */

    },

    docReady_winLoad: function() {

      /** Custom **/

        // ...

      /* * * * * * */

      /** Default **/
        fx_base.environment(); // base.js
        fx_formFields.validate(); // base.js
      /* * * * * * */
    },

    resize: function() {

      /** Custom **/

        // ...

      /* * * * * * */

      /** Default **/

         // ...

      /* * * * * * */
    }

  }

/* * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * */
/* Do not change or add nothing!!! */
/* * * * * * * * * * * * * * * * * */

  $(document).ready(function() {
    initialize.docReady();
    initialize.docReady_winLoad();
    initialize.resize();
    $(window).resize(function() {
      initialize.resize();
    });
  });
  $(window).load(function() {
    initialize.winLoad();
    initialize.docReady_winLoad();
    initialize.resize();
    $(window).resize(function() {
      initialize.resize();
    });
  });

/* * * * * * * * * * * * * * * * * */
