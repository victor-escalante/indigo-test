/* Custom functions on bottom /*

/* * * * * * * * * */
/* Base            */
/* * * * * * * * * */

  fx_base = {

    _construct: function() {
      _bThis = this;

      //--

      $footer          = $('footer');
      $html            = $('html');
      $smainContainer  = $('.smain-container');
    },

    _setEnvironment: function() {

      /* This function defines the height of
        the main container by placing the footer
        at the bottom of the site, use the corresponding
        classes according to your needs. */

      _bThis.getSizes();

      switch(true) {

        /*Classes for smain-container:

        min-screen -> Default setting, this class doesn't exist
        max-screen
        min-full-screen
        max-full-screen*/

        //----------------

        // Max height // With Footer
        case ($smainContainer.hasClass('max-screen')):
          $smainContainer
          .css({
            'height': siteHeightWithFooter
          });
          if(smainContainerHeight <= smainContainerRealHeight && siteHeight <= smainContainerRealHeight) {
            $smainContainer
            .css({
              'min-height': smainContainerRealHeight
            });
          }
        break;

        // Min height // Without Footer
        case ($smainContainer.hasClass('min-full-screen')):
          $smainContainer
          .css({
            'min-height': siteHeightWithoutFooter
          });
        break;

        // Max height // Without Footer
        case ($smainContainer.hasClass('max-full-screen')):
          $smainContainer
          .css({
            'height': siteHeightWithoutFooter
          });
          if(smainContainerHeight <= smainContainerRealHeight && siteHeight <= smainContainerRealHeight) {
            $smainContainer
            .css({
              'min-height': smainContainerRealHeight - footerHeight,
            });
          }
        break;

        // Default // Min height // With Footer
        default:
          $smainContainer

          .css({
            'min-height': siteHeightWithFooter,
          });
        break;
      }
    },

    _setSizes: function() {

      footerHeight             = $footer.outerHeight();
      siteHeight               = $html.outerHeight();
      smainContainerHeight     = $smainContainer.outerHeight();
      smainContainerRealHeight = $smainContainer[0].scrollHeight;

      siteHeightWithoutFooter  = siteHeight;
      siteHeightWithFooter     = siteHeight - footerHeight;
    },

    environment: function() {
      this._construct();

      //--

      _bThis._setEnvironment();

      $(window)
      .resize(function() {
        _bThis._setEnvironment();
      });
    },

    getSizes: function() {
      this._construct();

      //--

      _bThis._setSizes();

      $(window)
      .resize(function() {
        _bThis._setSizes();
      });

      return {
        'footerHeight'             : footerHeight,
        'siteHeight'               : siteHeight,

        'smainContainerHeight'     : smainContainerHeight,
        'smainContainerRealHeight' : smainContainerRealHeight,

        'siteHeightWithoutFooter'  : siteHeightWithoutFooter,
        'siteHeightWithFooter'     : siteHeightWithFooter,
      };
    },

    getUrl: function() {
      path       = window.location.pathname;
      pathArray  = window.location.href.split( '/' );
      host       = pathArray[2];
      protocol   = pathArray[0];
      url        = protocol + '//' + host;

      //--

      getParams = window.location.search.substring(1).split('&');

      parameters = {} ;

      $.each(getParams, function(i, item) {
        itemArray = item.split('=');
        parameters[itemArray[0]] = itemArray[1];
      });

      //--

      return {
        'host'        : host,
        'path'        : path,
        'parameters'  : parameters,
        'protocol'    : protocol,
        'url'         : url,
      }
    },
  }

  var baseUrl = fx_base.getUrl().url; // -> Use This

/* * * * * * * * * */

/* * * * * * * * * */
/* messages        */
/* * * * * * * * * */

  /*Errors/Modal of $errors variable*/

  fx_messages = {

    _construct: function() {
      _msgThis = this;

      //--

      $messages   = $('.messages');
      $btn_close  = $('.messages .close');
      $alert      = $('.messages .alert');
    },

    _hide: function(event) {
      event = typeof event !== 'undefined' ? event : null;

      event.preventDefault();
      event.stopPropagation();

      $messages
      .stop()
      .fadeOut();
    },

    init: function() {
      this._construct();

      //--

      $messages
      .on('click', function(event) {
        _msgThis._hide(event);
      });

      $alert
      .on('click', function(event) {
        event.stopPropagation();
      });

      $btn_close
      .on('click', function(event) {
        _msgThis._hide(event);
      })
    },
  }

/* * * * * * * * * */

/* * * * * * * * * */
/* Proportionaly   */
/* * * * * * * * * */

  /*If you are using squares or rectangles,
  these functions can help maintain the minimum
  or maximum proportion of the container to which
  the class is assigned.
  call fx_proportional.make() in the view where necessary*/

  fx_proportional = {

    _construct: function() {
      _propoThis = this;

      //--

      $proportional = $('.proportional'); // -> Use this
    },

    _square: function() {
      $proportional
      .each(function(index) {
        $object = $(this);
        if($object.hasClass('square')) { // -> Use this
          proportional_width  = $object[0].getBoundingClientRect().width;
          $object
          .css('min-height', proportional_width);
        }
      });

      $proportional
      .each(function(index) {
        $object = $(this);
        if($object.hasClass('square-fit')) { // -> Use this
          proportional_width  = $object[0].getBoundingClientRect().width;
          $object
          .css('height', proportional_width);
        }
      });
    },

    _rectangle: function() {
      $proportional
      .each(function(index) {
        $object = $(this);
        if($object.hasClass('rectangle')) { // -> Use this
          proportional_width = Math.round($object[0].getBoundingClientRect().width / 2);
          $object
          .css('min-height', proportional_width);
        }
      });

      $proportional
      .each(function(index) {
        $object = $(this);
        if($object.hasClass('rectangle-fit')) { // -> Use this
          proportional_width = Math.round($object[0].getBoundingClientRect().width / 2);
          $object
          .css('height', proportional_width);
        }
      });
    },

    make: function() {
      this._construct();

      //--

      _propoThis._square();
      _propoThis._rectangle();

      $(window)
      .resize(function() {
        _propoThis._square();
        _propoThis._rectangle();
      });
    }
  }

/* * * * * * * * * * * */

/* * * * * * * * * */
/* Customs inputs  */
/* * * * * * * * * */

  /*If you are using custom inputs or you need to validate any of these
  call fx_inputs.validate() in app.js inside of docReady function.*/

/** Inputs **/

  fx_formFields = {

    _construct: function() {
      _ffThis = this;

      //--

      $fields       = $('input, select, textarea');
      $only_numbers = $('input[type="only_number"]'); // Use only_numbers as type of input
    },

    _isEmpty: function($input_field) {
      //-- active
      if($input_field.val()) {
        $input_field
        .addClass('active');
      }
      else{
        $input_field
        .removeClass('active');
      }
      //--
    },

    _validateOnlyNumbers: function($input_field, event) {
      var a = [];
      var k = event.which;

      for (i = 48; i < 58; i++) { // 0 - 9
        a.push(i);
      }

      a.push(13); // Enter
      a.push(46); // Comma
      a.push(44); // Dot
      a.push(08); // Backspace

      if (!(a.indexOf(k)>=0)) {
        event.preventDefault();
      }
    },

    validate: function() {
      this._construct();

      //--

      $only_numbers
      .keypress(function(event) {
        _ffThis._validateOnlyNumbers($(this), event);
      });

      $fields
      .each(function(index) {
        _ffThis._isEmpty($(this));
      });

      $fields
      .on('change', function(event) {
        _ffThis._isEmpty($(this));
      });
    }
  }

/* * * * * * * * * * * */

/* * * * * * * * * */
/* Symmetrical     */
/* * * * * * * * * */

  /*If you want make symmetrical all items inside a container
  call fx_symmetrical.make() in the view where necessary*/

  fx_symmetrical = {

    _construct: function() {
      _symThis = this;

      //--

      $symmetrical_container = $('.symmetrical-container');
    },

    _symmetrical: function() {

      $symmetrical_container
      .each(function(i) {

        a = 0;
        b = 0;

        $symmetrical = $(this).find('.symmetrical');

        $symmetrical
        .each(function(j) {

          $symmetrical
          .css('height', 'auto');

          a = $(this).outerHeight();

          if(a >= b) {
            b = a;
          }

          $symmetrical
          .css('height', b);

        });

      });
    },

    make: function() {
      this._construct();

      //--

      _symThis._symmetrical();

      $(window)
      .resize(function() {
        _symThis._symmetrical();
      });

      $(window)
      .load(function() {
        _symThis._symmetrical();
      });
    }
  }

/* * * * * * * * * * * */
